<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\makanan;
use App\Models\kategori_makanan;

class MakananController extends Controller
{
    public function read(Request $request)
	{
		if($request->makanan) {
			$makanan = makanan::with('kategoriMakanan')->where('makanan', $request->makanan)->get();
		} else {
			$makanan = makanan::with('kategoriMakanan')->get();
		}

		return view('makanan', ['makanan' => $makanan]);
	}

	public function apiKategoriMakanan(Request $request)
	{
		$data = kategori_makanan::all();

		return $data;
	}

	public function apiMakanan(Request $request)
	{
		$data = makanan::all();

		return $data;
	}

	// ----------------

	public function store(Request $request)
    {
		$makanan = new kategori_makanan;

        $makanan->kategori_makanan = $request->kategori_makanan;

		$makanan->save();

		return redirect()->back();

    }
}
