<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kategori_makanan extends Model
{
    use HasFactory;

	protected $table = 'kategori_makanan';

	protected $fillable = [
        'kategori_makanan_id',
        'kategori_makanan',
    ];

	public function makanan()
	{
		return $this->hasMany(makanan::class, 'kategori_makanan_id', 'kategori_makanan_id');
	}
}
