<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class makanan extends Model
{
    use HasFactory;

	protected $table = 'makanan';

	protected $fillable = [
        'makanan_id',
        'kategori_makanan_id',
		'makanan',
    ];

	public function kategoriMakanan()
	{
		return $this->hasOne(kategori_makanan::class, 'kategori_makanan_id', 'kategori_makanan_id');
	}
}
