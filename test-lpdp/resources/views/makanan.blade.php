
<form method="get" accept-charset="UTF-8">
	<input type="text" name="makanan"></input>

	<button type="submit">Filter</button>

</form>

<br /><br />

<table border='1'>
	<tr>
		<th>Kategori Makanan</th>
		<th>Makanan</th>
	</tr>

	@foreach ($makanan as $data)
	<tr>
		<td>{{ $data->kategoriMakanan->kategori_makanan }}</td>
		<td>{{ $data->makanan }}</td>
	</tr>
	@endforeach
</table>

