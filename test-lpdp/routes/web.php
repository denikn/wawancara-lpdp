<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MakananController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/kategori-makanan/read', [MakananController::class, 'read']);

Route::get('/kategori-makanan', [MakananController::class, 'apiKategoriMakanan']);
Route::get('/makanan', [MakananController::class, 'apiMakanan']);

Route::get('/kategori-makanan-create', function () {
    return view('kategori-makanan');
});

Route::post('/kategori-makanan-create', [MakananController::class, 'store']);
